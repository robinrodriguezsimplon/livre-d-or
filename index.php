<?php
  session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Livre d'Or</title>
    <link rel="stylesheet" href="./node_modules/bootstrap/dist/css/bootstrap.css">
    <link rel="stylesheet" href="./node_modules/animate.css/animate.css">
    <link rel="stylesheet" href="./style.css">
</head>
<body>

<div id="grande" class="">
    <div id="title" class="container">
        <h1>Livre d'or</h1>
    </div>

    <form class="container mt-4" action="" method="post">
        <div id="form" class="container form-group">
            <input class="form-control m-2" id="idnom" type="text" name="name" placeholder="Nom" size="25" required>
            <textarea class="form-control m-2" id="idmessage" name="message" rows="5" cols="47" placeholder="Message" required></textarea>
            <input id='btn' class="btn btn-primary mb-4" type="submit" name="submit" value="Envoyer">
        </div>
    </form>
</div>

<div id="messages">
    <?php
        if ($_POST['name'] === $_SESSION['lastname']) {
        } else {
            if (isset($_POST['submit'])) {
            $file = 'fichier.txt';
            $handle = fopen($file, "a");
            $date= time();
            $dateM = date("m/d/Y à H:i", $date);
            $login = htmlspecialchars($_POST["name"]);
            $message = htmlspecialchars($_POST["message"]);
            $current = '<div class="post container"><hr><span class="livredor-nom">De <b> ' . $login . ' </b></span>';
            $current .= '<span class="livredor-date"> <i>le ' . $dateM . '</i> </span>';
            $current .= '<p class="livredor-message"> ' . $message . ' <p><hr></div>' . "\n";
            $truc = file_put_contents($file, $current,  FILE_APPEND);
            $_SESSION['lastname'] = $_POST['name'];
            };
        };    
            $homepage = file_get_contents('fichier.txt');
            $contents = fread($handle, filesize($file));
            echo $homepage;
            fclose($handle);
    ?>
</div>

</body>
</html>